.PHONY: server production

make:
	pipenv install --dev
	DJANGO_SETTINGS_MODULE=insaction.settings.dev pipenv run make configure

configure: blog/*.py mgmt/*.py website/*.py
	./manage.py makemigrations blog mgmt website
	./manage.py migrate
	./manage.py loaddata data/fixtures.json
	touch now.json

server:
    DJANGO_SETTINGS_MODULE=insaction.settings.dev make configure
	DJANGO_SETTINGS_MODULE=insaction.settings.dev ./manage.py runserver 0.0.0.0:8000

production: configure
	gunicorn --config gunicorn.py insaction.wsgi:application

tests:
	pipenv run ./manage.py test blog website

coverage:
	pipenv run coverage run --source='.' manage.py test
	
cov-xml: coverage
	pipenv run coverage xml

cov-html: coverage
	pipenv run coverage html

cov-codacy: cov-xml
	pipenv run python-codacy-coverage -r coverage.xml
